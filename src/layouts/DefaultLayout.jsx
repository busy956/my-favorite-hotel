import React from 'react';
import {Link} from "react-router-dom";

const DefaultLayout = ({ children }) => {
    return (
        // 프레그먼트
        <>
            <div>
                <h1>헤더</h1>
                <nav>
                    <Link to="/">main </Link>
                    |
                    <Link to="/myHotel"> 찜목록</Link>
                </nav>
            </div>
            <main>{children}</main>
            <footer>footer</footer>
        </>
    );
};

export default DefaultLayout;