import React, {useState, useEffect} from "react";

const HotelList = () => {
    // 호텔 목록
    // JSON이 들어간 배열
    const [hotels, setHotels] = useState([]);
    // 찜 목록
    // 로컬스토리지에서 key가 favorites인 문자열 데이터를 JSON으로 바꿔서 가져온다
    const [favorites, setFavorites] = useState(JSON.parse(localStorage.getItem('favorites')) || []);

    // 생성될 때, 업데이트될 때, 종료될 때 쓰인다
    // deps를 보면 생성인지 업데이트인지 알 수 있다(안에 뭐가 있으면 그것을 감시한다 -> 바뀌면 업데이트한다)
    // 생성되자마자 데이터를 가져온다 (호텔 목록 초기화....)
    useEffect(() => {
        // 전화기 데이터 좀 주세요(주소를 통해 데이터를 볼 수 있다)
        fetch("http://localhost:3000/hotels.json")
            // 데이터가 성공적으로 오면 결과물을 json으로 줘
            .then(res => res.json())
            // json으로 받아온 결과물을 호텔 목록에 세팅해줘
            .then(data => setHotels(data))
    }, []);

    // 업데이트될 때 (수정 hook)
    // favorites를 계속 쳐다보고 있겠다
    // 찜 목록이 바뀔때마다 사본을 직렬화해서 문자열로 바꿔서 로컬스토리지에 넣는다
    useEffect(() => {
        localStorage.setItem('favorites', JSON.stringify(favorites))
    }, [favorites]);

    const addToFavorites = (hotels) => {
        if (favorites.find(fav => fav.id === hotels.id)) {
            setFavorites(favorites.filter(fav => fav.id !== hotels.id))
        } else {
            setFavorites([...favorites, hotels])
        }
    }

    // 찜 목록에 찜한 호텔을 더해주겠다
    // 더해지면 찜 목록이 바뀐것이므로 수정 hook이 감지하고 작동한다
    const addFavoriteHotel = hotels => {
        // ...은 스프레드 문법
        setFavorites([...favorites, hotels])
    }

    // for문이나 map같은 반복문으로 호텔 목록을 출력한다
    // map은 기존에 있던 배열을 복사해서 새로운 배열을 만들어 낸다
    // key는 몇번인지 알기 위해서 필요하다(Lot 번호같이)
    // on은 행위를 한다는 것 (값을 넘기는 것이 아니다)
    // 행위를 하면 return이 있어야 언제 행위를 멈출지를 알 수 있다
    // addFavoriteHotel을 그대로 쓰면 언제 멈춰야 하는지를 몰라서 계속 돌아간다
    // 익명함수로 써주면 이 함수에 넘기고 나는 끝날게....
    return (
        <div>
            <h1>Hotel's List</h1>
            {hotels.map(hotels => (
                <div key={hotels.id}>
                    <p>호텔명 : {hotels.hotelName}</p>
                    <p>호텔 최저 가격 : {hotels.hotelMinPrice}원</p>
                    <button onClick={() => addFavoriteHotel(hotels)}>찜하기</button>
                </div>
            ))}
        </div>
    )
}

export default HotelList