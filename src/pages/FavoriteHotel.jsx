import React, {useEffect} from 'react';

const FavoriteHotel = () => {
    // 찜 목록을 로컬스토리지에서 불러와서 문자열에서 JSON으로 바꾸어서 가져와
    // 맨 처음 불러오면 아무 데이터도 없기 때문에 null을 가져오므로 빈 배열을 넣어준다
    const favorites = JSON.parse(localStorage.getItem("favorites")) || [];

    const deleteFavorite = () => {
        localStorage.removeItem('favorites');
    }

    return(
        <div>
            <h1>My Favorite Hotel</h1>
            {favorites.map(hotels => (
                <div key={hotels.id}>
                    <h2>{hotels.hotelName}</h2>
                    <p>{hotels.hotelMinPrice}</p>
                    <button onClick={() => deleteFavorite()}>삭제</button>
                </div>
            ))}
        </div>
    )
}

export default FavoriteHotel