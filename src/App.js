import './App.css';
import HotelList from "./pages/HotelList";
import FavoriteHotel from "./pages/FavoriteHotel";
import {Route, Routes} from "react-router-dom";
import DefaultLayout from "./layouts/DefaultLayout";

function App() {
    return (
        <div className="App">
            <Routes>
                <Route path="/"  element={<DefaultLayout><HotelList/></DefaultLayout>} />
                <Route path="/myHotel"  element={<DefaultLayout><FavoriteHotel/></DefaultLayout>} />
            </Routes>
        </div>
    );
}

export default App;
